﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Net.Sockets;
using System.Timers;

namespace LCHconfig
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static Mutex mtxcom1 = new Mutex();
        
        public enum MainMenu
        {
            NOTHING=0,
            SERIAL1,
            SERIAL2,
            SERIAL3,
            SERIAL4,
            RS,
            LAN,
            SERVICE

        }

        //MainMenu LeftMenu;

        MainMenu Screen = MainMenu.NOTHING;
        
        int ask_verinfo = 0;
        int confirm_verinfo = 0;
        int ask_rs = 0;

        int ask_serial_1 = 0;
        int ask_serial_2 = 0;
        int ask_serial_3 = 0;
        int ask_serial_4 = 0;

        int ask_lan = 0;
        int Write485_422_flag = 0;
        int WriteLan_flag = 0;

        int WriteSerial_flag_1 = 0;
        int WriteSerial_flag_2 = 0;
        int WriteSerial_flag_3 = 0;
        int WriteSerial_flag_4 = 0;

        int ReadDHCPLan_start = 0;
        int ReadDHCPLan_stop = 0;
        System.Timers.Timer myTimer;

        private void Form1_Load(object sender, EventArgs e)
        {
            //checkBox1.Appearance = Appearance.Button;
            myTimer = new System.Timers.Timer(5000);
            myTimer.Elapsed += OnTimeout;
            myTimer.AutoReset = true;
            myTimer.Enabled = false;
            try
            {
                string[] str = System.IO.Ports.SerialPort.GetPortNames();

                listBox1.Items.AddRange(str);
                listBox1.SetSelected(0, true);
                listBox1.SelectedItem = serialPort1.PortName;




            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("Подключите устройство к порту (нет ком порта)");
                Close();
            }
            this.Width = 420;
            this.Height = 395;

            panel1.Location = new Point(103, 10);
            panel2.Location = new Point(103, 10);
            panel3.Location = new Point(103, 10);
            panel5.Location = new Point(103, 10);
            panel6.Location = new Point(103, 10);
            panel7.Location = new Point(103, 10);
            panel8.Location = new Point(103, 10);
            panel9.Location = new Point(103, 10);
            panel10.Location = new Point(103, 10);
            panel3.Visible = false;
            panel1.Visible = true;
            panel2.Visible = false;
            panel5.Visible = false;
            panel6.Visible = false;
            panel7.Visible = false;
            panel8.Visible = false;
            panel9.Visible = false;
            panel10.Visible = false;
           

            toolStripMenuItem0.Visible = false;
            toolStripMenuItem01.Visible = false;
            toolStripMenuItem10.Visible = false;
            toolStripMenuItem11.Visible = false;
            toolStripMenuItem1.Visible = false;
            toolStripMenuItem2.Visible = false;
            toolStripMenuItem3.Visible = false;

            label1.Visible = false;
            label2.Visible = false;

            //// передаем в конструктор тип класса
            //XmlSerializer formatter1 = new XmlSerializer(typeof(Com));

            //// десериализация
            //using (FileStream fs1 = new FileStream("comone.xml", FileMode.OpenOrCreate))
            //{
            //    try
            //    {
            //        Com newPerson1 = (Com)formatter1.Deserialize(fs1);
            //        if (serialPort1.IsOpen)
            //        {
            //            serialPort1.Close();
            //            Thread.Sleep(1000);
            //        }
            //        //Console.WriteLine("Имя: {0} --- Возраст: {1}", newPerson.Name, newPerson.Baudrate);
            //        if (!serialPort1.IsOpen)
            //        {
            //            serialPort1.PortName = newPerson1.Name;
            //            serialPort1.BaudRate = newPerson1.Baudrate;//comboBox1.SelectedItem.ToString();
            //            serialPort1.Open();
            //            //Com1PortExist = 1;
            //            label1.Text = serialPort1.PortName;
            //            label2.Text = serialPort1.BaudRate.ToString();
            //            comboBox1.Text = serialPort1.BaudRate.ToString();
            //        }
            //    }
            //    catch (InvalidOperationException)
            //    {
            //        //MessageBox.Show("Настройте конфигурацию");
            //        //Close();
            //        groupBox1.BackColor = Color.LightPink;
            //    }
            //    catch (UnauthorizedAccessException)
            //    {
            //        //MessageBox.Show("Отказ в доступе к порту load");
            //        groupBox1.BackColor = Color.LightPink;
            //    }
            //    catch (ArgumentNullException)
            //    {
            //        ///пустота нет такого значения
            //        groupBox1.BackColor = Color.LightPink;
            //    }
            //    catch (IOException)
            //    {
            //        //MessageBox.Show("Отсутствует ком порт");
            //        groupBox1.BackColor = Color.LightPink;
            //        //Close();
            //    }
            //}






        }

        private void button1_Click(object sender, EventArgs e)//открыть порт
        {

            try
            {


                mtxcom1.WaitOne();
                if (serialPort1.IsOpen)
                {
                    serialPort1.Close();
                    Thread.Sleep(1000);
                }
                ///mtxcom1.ReleaseMutex();

                //mtxcom1.ReleaseMutex();
                //mtxcom1.WaitOne();
                serialPort1.PortName = listBox1.SelectedItem.ToString();
                serialPort1.BaudRate = int.Parse(comboBox1.SelectedItem.ToString());//comboBox1.SelectedItem.ToString();
                mtxcom1.ReleaseMutex();
                mtxcom1.WaitOne();

                serialPort1.Open();
                label1.Visible = true;
                label2.Visible = true;
                label1.Text = serialPort1.PortName;
                label2.Text = serialPort1.BaudRate.ToString();

                groupBox1.BackColor = System.Drawing.SystemColors.Control;


                if (serialPort1.IsOpen)
                {
                    ask_verinfo = 1;
                    var pollingThread = new Thread(Polling1);
                    pollingThread.IsBackground = true;
                    pollingThread.Start();
                }

                if (serialPort1.IsOpen)
                {
                    var pollingThread = new Thread(Polling);
                    pollingThread.IsBackground = true;
                    pollingThread.Start();
                }
                toolStripMenuItem0.Visible = true;
                toolStripMenuItem01.Visible = true;
                toolStripMenuItem10.Visible = true;
                toolStripMenuItem11.Visible = true;
                toolStripMenuItem1.Visible = true;
                toolStripMenuItem2.Visible = true;
                toolStripMenuItem3.Visible = true;

                //// объект для сериализации
                //Com com = new Com(serialPort1.PortName, serialPort1.BaudRate);
                ////Console.WriteLine("Объект создан");

                //// передаем в конструктор тип класса
                //XmlSerializer formatter = new XmlSerializer(typeof(Com));

                //// получаем поток, куда будем записывать сериализованный объект
                //using (FileStream fs = new FileStream("comone.xml", FileMode.Truncate))
                //{
                //    formatter.Serialize(fs, com);
                //}

                mtxcom1.ReleaseMutex();
              
                //if (ReadVerIvfo() == 1)
                //{                   
                //    Read485_422();
                //}

            }
            catch (Exception ex)//
            {
                groupBox1.BackColor = Color.LightPink;
                mtxcom1.ReleaseMutex();
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
         

            panel1.Visible = false;
            panel2.Visible = true;
            panel3.Visible = false;
            panel4.Visible = false;
            panel5.Visible = false;
            panel7.Visible = false;
            panel8.Visible = false;
            panel9.Visible = false;
            panel10.Visible = false;
            ask_rs = 1;
            label16.Text = "";

        }


        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
      
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = false;
            panel4.Visible = false;
            panel5.Visible = true;
            panel7.Visible = false;
            panel8.Visible = false;
            panel9.Visible = false;
            panel10.Visible = false;
            ask_lan = 1;
            label16.Text = "";
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = false;
            panel4.Visible = false;
            panel5.Visible = false;
            panel7.Visible = true;
            panel8.Visible = false;
            panel9.Visible = false;
            panel10.Visible = false;
            label16.Text = "";
        }



        static int CRC_16(byte[] bytes, int len, byte flag)//byte[] bytes//int CRC_16(byte* buffer, int len, byte flag)
        {
            int crc = 0xffff;
            int ind = 0;
            while (len > 0)
            {
                crc = crc16_update(crc, bytes[ind]);//crc=crc16_update(crc,*buffer)
                ind++;//buffer++
                len--;
            }
            if (flag > 0)
            {
                crc = crc & 0x0000;
                crc++;
            }
            return crc;
        }

        static int crc16_update(int crc, byte a)
        {
            int i;

            crc ^= a;
            for (i = 0; i < 8; ++i)
            {
                if ((crc & 1) != 0)
                    crc = (crc >> 1) ^ 0xA001;
                else
                    crc = (crc >> 1);
            }

            return crc;
        }
        [Serializable]

        public class Com
        {

            public string Name { get; set; }
            public int Baudrate { get; set; }




            // стандартный конструктор без параметров
            public Com()
            { }

            public Com(string name, int baudrate)
            {

                Name = name;
                Baudrate = baudrate;

            }
        }

        public class Ip
        {

            public string IpNum { get; set; }
            public int Port { get; set; }
            public int HostNum { get; set; }




            // стандартный конструктор без параметров
            public Ip()
            { }

            public Ip(string ipnum, int port, int hostnum)
            {

                IpNum = ipnum;
                Port = port;
                HostNum = hostnum;

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //string Method = comboBox2.SelectedIndex;
            if (comboBox2.SelectedIndex==0)
            {
                panel3.Visible = false;
                panel1.Visible = true;
            }
        }

        private void groupBox4_MouseCaptureChanged(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
           active422_notactive485();     
        }

        private void groupBox3_MouseCaptureChanged(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
            active485_notactive422();
        }

     
        private void active485_notactive422()
        {
            //textBox4.Enabled = false;
            //radioButton1.Enabled = false;
            //radioButton2.Enabled = false;

            //textBox2.Enabled = true;
            //textBox3.Enabled = true;
            //textBox9.Enabled = true;
            //textBox10.Enabled = true;

            //label8.ForeColor = System.Drawing.Color.LightGray;
            //label9.ForeColor = System.Drawing.Color.LightGray;
            //radioButton1.Text = "";
            //radioButton2.Text = "";


            textBox4.Invoke(new Action<bool>(t => textBox4.Enabled = t), false);
            radioButton1.Invoke(new Action<bool>(t => radioButton1.Enabled = t), false);
            radioButton2.Invoke(new Action<bool>(t => radioButton2.Enabled = t), false);

            textBox2.Invoke(new Action<bool>(t => textBox2.Enabled = t), true);
            textBox3.Invoke(new Action<bool>(t => textBox3.Enabled = t), true);
            textBox9.Invoke(new Action<bool>(t => textBox9.Enabled = t), true);
            textBox10.Invoke(new Action<bool>(t => textBox10.Enabled = t), true);

            label8.Invoke(new Action<Color>(t => label8.ForeColor = t), System.Drawing.Color.LightGray);
            label9.Invoke(new Action<Color>(t => label9.ForeColor = t), System.Drawing.Color.LightGray);


            radioButton1.Invoke(new Action(() => radioButton1.Text = ""));
            radioButton2.Invoke(new Action(() => radioButton2.Text = ""));




            foreach (Label label in groupBox3.Controls.OfType<Label>())
            {
                label.ForeColor = System.Drawing.Color.Black;
            }

            //groupBox3.Text = "RS-485";
            //groupBox4.Text = "";

            groupBox3.Invoke(new Action(() => groupBox3.Text = "RS-485"));
            groupBox4.Invoke(new Action(() => groupBox4.Text = ""));
        }
     

       private void active422_notactive485()
       {
            //textBox4.Enabled = true;
            //radioButton1.Enabled = true;
            //radioButton2.Enabled = true;
            textBox4.Invoke(new Action<bool>(t => textBox4.Enabled = t), true);
            radioButton1.Invoke(new Action<bool>(t => radioButton1.Enabled = t), true);
            radioButton2.Invoke(new Action<bool>(t => radioButton2.Enabled = t), true);
            //textBox2.Enabled = false;
            //textBox3.Enabled = false;
            //textBox9.Enabled = false;
            //textBox10.Enabled = false;
            textBox2.Invoke(new Action<bool>(t => textBox2.Enabled = t), false);
            textBox3.Invoke(new Action<bool>(t => textBox3.Enabled = t), false);
            textBox9.Invoke(new Action<bool>(t => textBox9.Enabled = t), false);
            textBox10.Invoke(new Action<bool>(t => textBox10.Enabled = t), false);

            //label8.ForeColor = System.Drawing.Color.Black;
            //label9.ForeColor = System.Drawing.Color.Black;
            label8.Invoke(new Action<Color>(t => label8.ForeColor = t), System.Drawing.Color.Black);
            label9.Invoke(new Action<Color>(t => label9.ForeColor = t), System.Drawing.Color.Black);
            //radioButton1.Text = "USB";
            //radioButton2.Text = "LAN";
            radioButton1.Invoke(new Action(() => radioButton1.Text = "USB"));
            radioButton2.Invoke(new Action(() => radioButton2.Text = "LAN"));
            //groupBox4.Text = "RS-422";
            //groupBox3.Text = "";
            groupBox3.Invoke(new Action(() => groupBox3.Text = ""));
            groupBox4.Invoke(new Action(() => groupBox4.Text = "RS-422"));

            foreach (Label label in groupBox3.Controls.OfType<Label>())
            {
                label.ForeColor = System.Drawing.Color.LightGray;
            }
       }

       private void textBox10_Click(object sender, EventArgs e)
       {
           label16.Enabled = true;
           label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
           label16.Text = "Save";
       }

       private void textBox9_Click(object sender, EventArgs e)
       {
           label16.Enabled = true;
           label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
           label16.Text = "Save";
       }

       private void textBox2_Click(object sender, EventArgs e)
       {
           label16.Enabled = true;
           label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
           label16.Text = "Save";
       }

       private void textBox3_Click(object sender, EventArgs e)
       {
           label16.Enabled = true;
           label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
           label16.Text = "Save";
       }
       private void textBox4_TextChanged(object sender, EventArgs e)
       {
           label16.Enabled = true;
           label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
           label16.Text = "Save";
       }


////////////////////////////////////////////////////////////////////////////////////////////

       private void textBox11_Click(object sender, EventArgs e)
       {
           label16.Enabled = true;
           label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
           label16.Text = "Save";
       }
       private void textBox12_Click(object sender, EventArgs e)
       {
           label16.Enabled = true;
           label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
           label16.Text = "Save";
       }
       private void textBox13_Click(object sender, EventArgs e)
       {
           label16.Enabled = true;
           label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
           label16.Text = "Save";
       }
       private void textBox14_Click(object sender, EventArgs e)
       {
           label16.Enabled = true;
           label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
           label16.Text = "Save";
       }
       private void textBox5_Click(object sender, EventArgs e)
       {
           label16.Enabled = true;
           label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
           label16.Text = "Save";
       }
       private void textBox6_Click(object sender, EventArgs e)
       {
           label16.Enabled = true;
           label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
           label16.Text = "Save";
       }
        /////////////////////////////////////////////////////////////////////////////////////////////
        //private void comboBox2_Click(object sender, EventArgs e)
        //{
        //    label16.Enabled = true;
        //    label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
        //    label16.Text = "Save";
        //}
        //private void comboBox3_Click(object sender, EventArgs e)
        //{
        //    label16.Enabled = true;
        //    label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
        //    label16.Text = "Save";
        //}
        //private void comboBox4_Click(object sender, EventArgs e)
        //{
        //    label16.Enabled = true;
        //    label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
        //    label16.Text = "Save";
        //}
        //private void comboBox5_Click(object sender, EventArgs e)
        //{
        //    label16.Enabled = true;
        //    label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
        //    label16.Text = "Save";
        //}
        //private void comboBox6_Click(object sender, EventArgs e)
        //{
        //    label16.Enabled = true;
        //    label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
        //    label16.Text = "Save";
        //}



        private void label16_Click(object sender, EventArgs e)
       {
           if (Screen == MainMenu.RS)
           {
                Write485_422_flag = 1;
              
           }

           if (Screen == MainMenu.LAN)
           {
                WriteLan_flag = 1;
            
           }
            if (Screen == MainMenu.SERIAL1)
            {
                WriteSerial_flag_1 = 1;
            }

            if (Screen == MainMenu.SERIAL2)
            {
                WriteSerial_flag_2 = 1;
            }
            if (Screen == MainMenu.SERIAL3)
            {
                WriteSerial_flag_3 = 1;
            }
            if (Screen == MainMenu.SERIAL4)
            {
                WriteSerial_flag_4 = 1;
            }
            //WriteLan()



            //Thread.Sleep(2000);
            //label16.Text = "";
            //label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
        }

       private void label19_Click(object sender, EventArgs e)
       {

       }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }

        private  void OnTimeout(Object source, ElapsedEventArgs e)
        {
            //Console.WriteLine("Timer expired");
            ReadDHCPLan_stop = 1;
            myTimer.Stop();
        }

        private void checkBox1_CheckStateChanged(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void textBox15_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void sERIALToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = false;

            panel3.Visible = true;
            panel8.Visible = false;
            panel9.Visible = false;
            panel10.Visible = false;

            panel4.Visible = false;
            panel5.Visible = false;
            panel7.Visible = false;
            ask_serial_1 = 1;
            label16.Text = "";
        }

        private void comboBox2_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox3_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox4_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox5_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox6_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }
        //---------------------------------------------------------------------
        private void comboBox11_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox10_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox9_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox8_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox7_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }
        //----------------------------------------------------------------------------------------------------
        private void comboBox16_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox15_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox14_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox13_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox12_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }
        //-----------------------------------------------------------------------------------
        private void comboBox21_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox20_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox19_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox18_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }

        private void comboBox17_Click(object sender, EventArgs e)
        {
            label16.Enabled = true;
            label16.ForeColor = System.Drawing.Color.DeepSkyBlue;
            label16.Text = "Save";
        }


        private void sERIAL2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = false;

            panel3.Visible = false;
            panel8.Visible = true;
            panel9.Visible = false;
            panel10.Visible = false;

            panel4.Visible = false;
            panel5.Visible = false;
            panel7.Visible = false;
            ask_serial_2 = 1;
            label16.Text = "";
        }

        private void sERIAL3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = false;

            panel3.Visible = false;
            panel8.Visible = false;
            panel9.Visible = true;
            panel10.Visible = false;

            panel4.Visible = false;
            panel5.Visible = false;
            panel7.Visible = false;
            ask_serial_3 = 1;
            label16.Text = "";
        }

        private void sERIAL4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = false;

            panel3.Visible = false;
            panel8.Visible = false;
            panel9.Visible = false;
            panel10.Visible = true;

            panel4.Visible = false;
            panel5.Visible = false;
            panel7.Visible = false;
            ask_serial_4 = 1;
            label16.Text = "";
        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox19_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        //private void checkBox1_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    try
        //    {


        //        mtxcom1.WaitOne();
        //        if (serialPort1.IsOpen)
        //        {
        //            serialPort1.Close();
        //            Thread.Sleep(1000);
        //        }

        //        serialPort1.PortName = listBox1.SelectedItem.ToString();
        //        serialPort1.BaudRate = int.Parse(comboBox1.SelectedItem.ToString());//comboBox1.SelectedItem.ToString();
        //        mtxcom1.ReleaseMutex();
        //        mtxcom1.WaitOne();

        //        serialPort1.Open();
        //        label1.Text = serialPort1.PortName;
        //        label2.Text = serialPort1.BaudRate.ToString();

        //        groupBox1.BackColor = System.Drawing.SystemColors.Control;


        //        mtxcom1.ReleaseMutex();


        //    }
        //    catch (Exception ex)//
        //    {
        //        groupBox1.BackColor = Color.LightPink;
        //        mtxcom1.ReleaseMutex();
        //    }
        //}

        //private void checkBox1_KeyUp(object sender, KeyEventArgs e)
        //{
        //    serialPort1.Close();
        //}




    }
}
