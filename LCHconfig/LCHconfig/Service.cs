﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.IO.Ports;


namespace LCHconfig
{
    public partial class Form1 : Form
    {
        string pathfile;
        int NumberPacket;
        //byte[] InBuffer = new byte[1024];
        byte symvol;
        uint CountBufferDebugIn;


        byte StartProshivka = 0;
        private void Polling()
        {
            while (true)
            {
                if (StartProshivka == 1)
                {
                    StartProshivka = 0;
                    //button1.Invoke(new Action<bool>(t => button1.Enabled = t), false);
                    //button3.Invoke(new Action<bool>(t => button3.Enabled = t), false);
                    Proshivka();
                    //button1.Invoke(new Action<bool>(t => button1.Enabled = t), true);
                    //button3.Invoke(new Action<bool>(t => button3.Enabled = t), true);
                   
                }
                Thread.Sleep(100);
            }
        }
        private void button3_Click(object sender, EventArgs e)//кнопка запись
        {
            if (textBox1.Text != "")
            {
                StartProshivka = 1;
                button3.Enabled = false;
                toolStripMenuItem1.Visible = false;
                toolStripMenuItem2.Visible = false;
            }
        }

        public void Proshivka()
        {
            int DataSize = 128;//1024
            byte[] array_tosend = new byte[DataSize];// byte[] array_tosend = new byte[1024]; 
            byte[] array_tosend_1 = new byte[DataSize];
            byte[] array_tosend_2 = new byte[DataSize];
            byte[] array_tosend_3 = new byte[DataSize];
            byte[] array_tosend_4 = new byte[DataSize];
            byte[] array_tosend_5 = new byte[DataSize];
            byte[] array_tosend_6 = new byte[DataSize];
            byte[] array_tosend_7 = new byte[DataSize];

            int Count_Position = 0;
            //int OverFlow;
            int EndWriting = 0;
            int NumberStruct = 0;
            int[] NumbersStruct = new int[7];


            //byte[] InBuffer = new byte[1024];
            //byte symvol;
            //uint CountBufferDebugIn;

            //string pathfile;
            uint group_128_number = 0;

            try
            {




                using (FileStream fstream = File.OpenRead(@pathfile))//buildsum
                {
                    // преобразуем строку в байты

                    byte[] array = new byte[fstream.Length];//byte[] array = new byte[fstream.Length];//20
                    // считываем данные
                    fstream.Read(array, 0, array.Length);
                    uint crc_real;
                    crc_real = 0xffffffff;

                    int progress;

                    while ((Count_Position + DataSize <= array.Length) && (EndWriting == 0))//
                    {

                        progress = ((Count_Position + DataSize) * 100) / array.Length;
                        //progressBar1.Value = progress;
                        progressBar1.Invoke(new Action<int>(t => progressBar1.Value = t), progress);

                        int u = 0;
                        ArraySegment<byte> segment = new ArraySegment<byte>(array, Count_Position, DataSize);
                        for (int i = segment.Offset; i < segment.Offset + segment.Count; i++)
                        {
                            array_tosend[u] = segment.Array[i];
                            u++;
                        }
                        Count_Position += DataSize;

                        for (int i = 0; i < 128; i++)
                        {
                            crc_real = crc32_update(crc_real, array_tosend[i]);
                        }

                        NumberPacket++; NumberStruct++;
                        if (group_128_number == 0)
                        {
                            NumbersStruct[0] = NumberStruct;
                            group_128_number++;
                            for (int i = 0; i < 128; i++)
                            {
                                array_tosend_1[i] = array_tosend[i];
                            }
                        }
                        else
                            if (group_128_number == 1)
                            {
                                NumbersStruct[1] = NumberStruct;
                                group_128_number++;
                                for (int i = 0; i < 128; i++)
                                {
                                    array_tosend_2[i] = array_tosend[i];
                                }

                            }
                            else
                                if (group_128_number == 2)
                                {
                                    NumbersStruct[2] = NumberStruct;
                                    group_128_number++;
                                    for (int i = 0; i < 128; i++)
                                    {
                                        array_tosend_3[i] = array_tosend[i];
                                    }
                                }
                                else
                                    if (group_128_number == 3)
                                    {
                                        NumbersStruct[3] = NumberStruct;
                                        group_128_number++;
                                        for (int i = 0; i < 128; i++)
                                        {
                                            array_tosend_4[i] = array_tosend[i];
                                        }
                                    }
                                    else
                                        if (group_128_number == 4)
                                        {
                                            NumbersStruct[4] = NumberStruct;
                                            group_128_number++;
                                            for (int i = 0; i < 128; i++)
                                            {
                                                array_tosend_5[i] = array_tosend[i];
                                            }
                                        }
                                        else
                                            if (group_128_number == 5)
                                            {
                                                NumbersStruct[5] = NumberStruct;
                                                group_128_number++;
                                                for (int i = 0; i < 128; i++)
                                                {
                                                    array_tosend_6[i] = array_tosend[i];
                                                }
                                            }
                                            else
                                                if (group_128_number == 6)
                                                {
                                                    NumbersStruct[6] = NumberStruct;
                                                    group_128_number++;
                                                    for (int i = 0; i < 128; i++)
                                                    {
                                                        array_tosend_7[i] = array_tosend[i];
                                                    }
                                                    byte result;
                                                    result = Send_Data(array_tosend_1, array_tosend_2, array_tosend_3, array_tosend_4, array_tosend_5, array_tosend_6, array_tosend_7, NumbersStruct, group_128_number);
                                                    group_128_number = 0;
                                                    if (result != 1)
                                                    {
                                                        progressBar1.Invoke(new Action<int>(t => progressBar1.Value = t), 0);                                                       
                                                        toolStripMenuItem1.Visible = true;
                                                        toolStripMenuItem2.Visible = true;
                                                        return;
                            }
                                                }


                    }
                    byte result1 = 0;

                    if (group_128_number != 0)
                        result1 = Send_Data(array_tosend_1, array_tosend_2, array_tosend_3, array_tosend_4, array_tosend_5, array_tosend_6, array_tosend_7, NumbersStruct, group_128_number);

                    if (result1 == 1)
                        //button3.Visible = true;
                        button3.Invoke(new Action<bool>(t => button3.Enabled = t), true);
                    else
                    if((result1 != 1)&&(group_128_number != 0))
                    {
                        progressBar1.Invoke(new Action<int>(t => progressBar1.Value = t), 0);
                        toolStripMenuItem1.Visible = true;
                        toolStripMenuItem2.Visible = true;
                        return;
                    }
                    toolStripMenuItem1.Visible = true;
                    toolStripMenuItem2.Visible = true;
                    Count_Position = 0;

                    NumberPacket++;
                    Send_CRC32(crc_real, array.Length);

                    NumberPacket++;
                    Send_BOOTWRITE();
                    NumberPacket++;

                    Application.Exit();


                }
            }
            catch (ArgumentException)
            {

            }
            catch
            {

            }
        }

        public byte Send_Data(byte[] array_1, byte[] array_2, byte[] array_3, byte[] array_4, byte[] array_5, byte[] array_6, byte[] array_7, int[] NumbersStruct, uint last_group)
        {
            int u = 0;
            byte result = 1;
            byte[] InBuffer = new byte[1024];
            byte[] array_protocol = new byte[(array_1.Length + 6) * last_group + 14];
            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            //array_protocol[4] = (byte)((array.Length + 20));
            //array_protocol[5] = (byte)((array.Length + 20) >> 8);
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x03;//функция записи
            array_protocol[11] = (byte)last_group;//количество индексированных команд
            array_protocol[12] = 0xFA;//Индекс команды
            array_protocol[13] = 0xFF;//BOOT_SER
            array_protocol[14] = (byte)((NumbersStruct[0]));
            array_protocol[15] = (byte)((NumbersStruct[0]) >> 8);
            array_protocol[16] = (byte)((array_1.Length));
            array_protocol[17] = (byte)((array_1.Length) >> 8);
            int i;


            for (i = 18; i < 128 + 18; i++)
            {
                array_protocol[i] = array_1[u];
                u++;
            }

            if (last_group == 1)
            {
                array_protocol[4] = (byte)((array_1.Length + 20));
                array_protocol[5] = (byte)((array_1.Length + 20) >> 8);

                array_protocol[14] = (byte)((NumbersStruct[0]));
                array_protocol[15] = (byte)((NumbersStruct[0]) >> 8);

                uint get_crc = CRC_16(array_protocol, array_1.Length + 18);
                array_protocol[i] = (byte)((get_crc));
                i++;
                array_protocol[i] = (byte)((get_crc) >> 8);
                i++;

                serialPort1.Write(array_protocol, 0, array_1.Length + 20);
                serialPort1.ReadTimeout = 800;//
                try
                {
                    for (byte v = 0; v < 255; v++)
                    {
                        InBuffer[v] = (byte)serialPort1.ReadByte();
                        symvol = InBuffer[v];
                        CountBufferDebugIn = v;
                        serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                    }
                }
                catch
                {
                    if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                        result = 1;
                    else
                        result = 0;
                }
                return result;
            }

            array_protocol[i] = 0xFA;//Индекс команды 
            i++;
            array_protocol[i] = 0xFF;//BOOT_SER
            i++;
            array_protocol[i] = (byte)((NumbersStruct[1]));
            i++;
            array_protocol[i] = (byte)((NumbersStruct[1]) >> 8);
            i++;
            array_protocol[i] = (byte)((array_2.Length));
            i++;
            array_protocol[i] = (byte)((array_2.Length) >> 8);
            i++;

            u = 0;
            for (int v = 0; v < 128; v++)
            {
                array_protocol[i] = array_2[u];
                u++;
                i++;
            }

            if (last_group == 2)
            {
                array_protocol[4] = (byte)((array_1.Length + array_2.Length + 20 + 6));
                array_protocol[5] = (byte)((array_1.Length + array_2.Length + 20 + 6) >> 8);



                uint get_crc = CRC_16(array_protocol, array_1.Length + array_2.Length + 18 + 6);
                array_protocol[i] = (byte)((get_crc));
                i++;
                array_protocol[i] = (byte)((get_crc) >> 8);
                i++;

                serialPort1.Write(array_protocol, 0, array_1.Length + array_2.Length + 20 + 6);
                serialPort1.ReadTimeout = 800;//
                try
                {
                    for (byte v = 0; v < 255; v++)
                    {
                        InBuffer[v] = (byte)serialPort1.ReadByte();
                        symvol = InBuffer[v];
                        CountBufferDebugIn = v;
                        serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                    }
                }
                catch
                {
                    if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                        result = 1;
                    else
                        result = 0;
                }
                return result;
            }

            array_protocol[i] = 0xFA;//Индекс команды 
            i++;
            array_protocol[i] = 0xFF;//BOOT_SER
            i++;
            array_protocol[i] = (byte)((NumbersStruct[2]));
            i++;
            array_protocol[i] = (byte)((NumbersStruct[2]) >> 8);
            i++;
            array_protocol[i] = (byte)((array_3.Length));
            i++;
            array_protocol[i] = (byte)((array_3.Length) >> 8);
            i++;

            u = 0;
            for (int v = 0; v < 128; v++)
            {
                array_protocol[i] = array_3[u];
                u++;
                i++;
            }

            if (last_group == 3)
            {
                array_protocol[4] = (byte)((array_1.Length + array_2.Length + array_3.Length + 20 + 6 + 6));
                array_protocol[5] = (byte)((array_1.Length + array_2.Length + array_3.Length + 20 + 6 + 6) >> 8);



                uint get_crc = CRC_16(array_protocol, array_1.Length + array_2.Length + array_3.Length + 18 + 6 + 6);
                array_protocol[i] = (byte)((get_crc));
                i++;
                array_protocol[i] = (byte)((get_crc) >> 8);
                i++;

                serialPort1.Write(array_protocol, 0, array_1.Length + array_2.Length + array_3.Length + 20 + 6 + 6);
                serialPort1.ReadTimeout = 800;//
                try
                {
                    for (byte v = 0; v < 255; v++)
                    {
                        InBuffer[v] = (byte)serialPort1.ReadByte();
                        symvol = InBuffer[v];
                        CountBufferDebugIn = v;
                        serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                    }
                }
                catch
                {
                    if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                        result = 1;
                    else
                        result = 0;
                }
                return result;
            }

            array_protocol[i] = 0xFA;//Индекс команды 
            i++;
            array_protocol[i] = 0xFF;//BOOT_SER
            i++;
            array_protocol[i] = (byte)((NumbersStruct[3]));
            i++;
            array_protocol[i] = (byte)((NumbersStruct[3]) >> 8);
            i++;
            array_protocol[i] = (byte)((array_4.Length));
            i++;
            array_protocol[i] = (byte)((array_4.Length) >> 8);
            i++;

            u = 0;
            for (int v = 0; v < 128; v++)
            {
                array_protocol[i] = array_4[u];
                u++;
                i++;
            }

            if (last_group == 4)
            {
                array_protocol[4] = (byte)((array_1.Length + array_2.Length + array_3.Length + array_4.Length + 20 + 6 + 6 + 6));
                array_protocol[5] = (byte)((array_1.Length + array_2.Length + array_3.Length + array_4.Length + 20 + 6 + 6 + 6) >> 8);



                uint get_crc = CRC_16(array_protocol, array_1.Length + array_2.Length + array_3.Length + array_4.Length + 18 + 6 + 6 + 6);
                array_protocol[i] = (byte)((get_crc));
                i++;
                array_protocol[i] = (byte)((get_crc) >> 8);
                i++;

                serialPort1.Write(array_protocol, 0, array_1.Length + array_2.Length + array_3.Length + array_4.Length + 20 + 6 + 6 + 6);
                serialPort1.ReadTimeout = 800;//
                try
                {
                    for (byte v = 0; v < 255; v++)
                    {
                        InBuffer[v] = (byte)serialPort1.ReadByte();
                        symvol = InBuffer[v];
                        CountBufferDebugIn = v;
                        serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                    }
                }
                catch
                {
                    if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                        result = 1;
                    else
                        result = 0;
                }
                return result;
            }

            array_protocol[i] = 0xFA;//Индекс команды 
            i++;
            array_protocol[i] = 0xFF;//BOOT_SER
            i++;
            array_protocol[i] = (byte)((NumbersStruct[4]));
            i++;
            array_protocol[i] = (byte)((NumbersStruct[4]) >> 8);
            i++;
            array_protocol[i] = (byte)((array_5.Length));
            i++;
            array_protocol[i] = (byte)((array_5.Length) >> 8);
            i++;

            u = 0;
            for (int v = 0; v < 128; v++)
            {
                array_protocol[i] = array_5[u];
                u++;
                i++;
            }

            if (last_group == 5)
            {
                array_protocol[4] = (byte)((array_1.Length + array_2.Length + array_3.Length + array_4.Length + array_5.Length + 20 + 6 + 6 + 6 + 6));
                array_protocol[5] = (byte)((array_1.Length + array_2.Length + array_3.Length + array_4.Length + array_5.Length + 20 + 6 + 6 + 6 + 6) >> 8);



                uint get_crc = CRC_16(array_protocol, array_1.Length + array_2.Length + array_3.Length + array_4.Length + array_5.Length + 18 + 6 + 6 + 6 + 6);
                array_protocol[i] = (byte)((get_crc));
                i++;
                array_protocol[i] = (byte)((get_crc) >> 8);
                i++;

                serialPort1.Write(array_protocol, 0, array_1.Length + array_2.Length + array_3.Length + array_4.Length + array_5.Length + 20 + 6 + 6 + 6 + 6);
                serialPort1.ReadTimeout = 800;//
                try
                {
                    for (byte v = 0; v < 255; v++)
                    {
                        InBuffer[v] = (byte)serialPort1.ReadByte();
                        symvol = InBuffer[v];
                        CountBufferDebugIn = v;
                        serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                    }
                }
                catch
                {
                    if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                        result = 1;
                    else
                        result = 0;
                }
                return result;
            }

            array_protocol[i] = 0xFA;//Индекс команды 
            i++;
            array_protocol[i] = 0xFF;//BOOT_SER
            i++;
            array_protocol[i] = (byte)((NumbersStruct[5]));
            i++;
            array_protocol[i] = (byte)((NumbersStruct[5]) >> 8);
            i++;
            array_protocol[i] = (byte)((array_6.Length));
            i++;
            array_protocol[i] = (byte)((array_6.Length) >> 8);
            i++;

            u = 0;
            for (int v = 0; v < 128; v++)
            {
                array_protocol[i] = array_6[u];
                u++;
                i++;
            }

            if (last_group == 6)
            {
                array_protocol[4] = (byte)((array_1.Length + array_2.Length + array_3.Length + array_4.Length + array_5.Length + array_6.Length + 20 + 6 + 6 + 6 + 6 + 6));
                array_protocol[5] = (byte)((array_1.Length + array_2.Length + array_3.Length + array_4.Length + array_5.Length + array_6.Length + 20 + 6 + 6 + 6 + 6 + 6) >> 8);



                uint get_crc = CRC_16(array_protocol, array_1.Length + array_2.Length + array_3.Length + array_4.Length + array_5.Length + array_6.Length + 18 + 6 + 6 + 6 + 6 + 6);
                array_protocol[i] = (byte)((get_crc));
                i++;
                array_protocol[i] = (byte)((get_crc) >> 8);
                i++;

                serialPort1.Write(array_protocol, 0, array_1.Length + array_2.Length + array_3.Length + array_4.Length + array_5.Length + array_6.Length + 20 + 6 + 6 + 6 + 6 + 6);
                serialPort1.ReadTimeout = 800;//
                try
                {
                    for (byte v = 0; v < 255; v++)
                    {
                        InBuffer[v] = (byte)serialPort1.ReadByte();
                        symvol = InBuffer[v];
                        CountBufferDebugIn = v;
                        serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                    }
                }
                catch
                {
                    if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                        result = 1;
                    else
                        result = 0;
                }
                return result;
            }

            array_protocol[i] = 0xFA;//Индекс команды 
            i++;
            array_protocol[i] = 0xFF;//BOOT_SER
            i++;
            array_protocol[i] = (byte)((NumbersStruct[6]));
            i++;
            array_protocol[i] = (byte)((NumbersStruct[6]) >> 8);
            i++;
            array_protocol[i] = (byte)((array_7.Length));
            i++;
            array_protocol[i] = (byte)((array_7.Length) >> 8);
            i++;

            u = 0;
            for (int v = 0; v < 128; v++)
            {
                array_protocol[i] = array_7[u];
                u++;
                i++;
            }

            if (last_group == 7)
            {
                array_protocol[4] = (byte)((array_1.Length + array_2.Length + array_3.Length + array_4.Length + array_5.Length + array_6.Length + array_7.Length + 20 + 6 + 6 + 6 + 6 + 6 + 6));
                array_protocol[5] = (byte)((array_1.Length + array_2.Length + array_3.Length + array_4.Length + array_5.Length + array_6.Length + array_7.Length + 20 + 6 + 6 + 6 + 6 + 6 + 6) >> 8);



                uint get_crc = CRC_16(array_protocol, array_1.Length + array_2.Length + array_3.Length + array_4.Length + array_5.Length + array_6.Length + array_7.Length + 18 + 6 + 6 + 6 + 6 + 6 + 6);
                array_protocol[i] = (byte)((get_crc));
                i++;
                array_protocol[i] = (byte)((get_crc) >> 8);
                i++;

                serialPort1.Write(array_protocol, 0, array_1.Length + array_2.Length + array_3.Length + array_4.Length + array_5.Length + array_6.Length + array_7.Length + 20 + 6 + 6 + 6 + 6 + 6 + 6);

                serialPort1.ReadTimeout = 800;//
                try
                {
                    for (byte v = 0; v < 255; v++)
                    {
                        InBuffer[v] = (byte)serialPort1.ReadByte();
                        symvol = InBuffer[v];
                        CountBufferDebugIn = v;
                        serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума                     

                    }
                }
                catch
                {
                    if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                        result = 1;
                    else
                        result = 0;
                }
                return result;
            }

            return result;

        }


        void Send_CRC32(uint crc_real, int length)
        {
            byte result = 1;
            crc_real = ~crc_real;
            byte[] InBuffer = new byte[1024];
            byte[] array_protocol = new byte[28];
            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            array_protocol[4] = 28;// размер всего пакета
            array_protocol[5] = 0;
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x03;//функция записи
            array_protocol[11] = 0x01;//количество индексированных команд
            array_protocol[12] = 0xF7;//Индекс команды
            array_protocol[13] = 0xFF;//BOOT_CRC32
            array_protocol[14] = 1;// струк-
            array_protocol[15] = 0;// тура
            array_protocol[16] = 8;// кол-во
            array_protocol[17] = 0;// данных в структуре

            array_protocol[18] = (byte)((crc_real));
            array_protocol[19] = (byte)((crc_real) >> 8);
            array_protocol[20] = (byte)((crc_real) >> 16);
            array_protocol[21] = (byte)((crc_real) >> 24);

            array_protocol[22] = (byte)((length));
            array_protocol[23] = (byte)((length) >> 8);
            array_protocol[24] = (byte)((length) >> 16);
            array_protocol[25] = (byte)((length) >> 24);

            uint get_crc = CRC_16(array_protocol, 26);

            array_protocol[26] = (byte)((get_crc));
            array_protocol[27] = (byte)((get_crc) >> 8);

            serialPort1.Write(array_protocol, 0, 28);
            serialPort1.ReadTimeout = 800;//
            try
            {
                for (byte v = 0; v < 255; v++)
                {
                    InBuffer[v] = (byte)serialPort1.ReadByte();
                    symvol = InBuffer[v];
                    CountBufferDebugIn = v;
                    //serialPort1.ReadTimeout = 10;//первый байт принят уменьшаем таймаут до минимума
                }
            }
            catch
            {
                if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                    result = 1;
                else
                    result = 0;
            }

            //return result; 


        }

        void Send_BOOTWRITE()
        {
            byte result = 1;
            byte[] InBuffer = new byte[1024];
            byte[] array_protocol = new byte[21];
            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            array_protocol[4] = 21;// размер всего пакета
            array_protocol[5] = 0;
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x03;//функция записи
            array_protocol[11] = 0x01;//количество индексированных команд
            array_protocol[12] = 0xF8;//Индекс команды
            array_protocol[13] = 0xFF;//BOOT_CRC32
            array_protocol[14] = 1;// струк-
            array_protocol[15] = 0;// тура
            array_protocol[16] = 1;// кол-во
            array_protocol[17] = 0;// данных в структуре

            array_protocol[18] = 0x31;


            uint get_crc = CRC_16(array_protocol, 19);

            array_protocol[19] = (byte)((get_crc));
            array_protocol[20] = (byte)((get_crc) >> 8);
            serialPort1.Write(array_protocol, 0, 21);
            serialPort1.ReadTimeout = 800;//
            try
            {
                for (byte v = 0; v < 255; v++)
                {
                    InBuffer[v] = (byte)serialPort1.ReadByte();
                    symvol = InBuffer[v];
                    CountBufferDebugIn = v;
                    //serialPort1.ReadTimeout = 10;//первый байт принят уменьшаем таймаут до минимума
                }
            }
            catch
            {
                if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                    result = 1;
                else
                    result = 0;
            }

            int a = 1;
        }
        //////////////////////////////////////////////////////////////////////////////
        void Send_REBOOT()
        {
            byte result = 1;
            byte[] InBuffer = new byte[1024];
            byte[] array_protocol = new byte[21];
            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            array_protocol[4] = 21;// размер всего пакета
            array_protocol[5] = 0;
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x03;//функция записи
            array_protocol[11] = 0x01;//количество индексированных команд
            array_protocol[12] = 0xF1;//Индекс команды
            array_protocol[13] = 0xFF;//REBOOT
            array_protocol[14] = 1;// струк-
            array_protocol[15] = 0;// тура
            array_protocol[16] = 1;// кол-во
            array_protocol[17] = 0;// данных в структуре

            array_protocol[18] = 0x31;


            uint get_crc = CRC_16(array_protocol, 19);

            array_protocol[19] = (byte)((get_crc));
            array_protocol[20] = (byte)((get_crc) >> 8);
            serialPort1.Write(array_protocol, 0, 21);
            serialPort1.ReadTimeout = 800;//
            try
            {
                for (byte v = 0; v < 255; v++)
                {
                    InBuffer[v] = (byte)serialPort1.ReadByte();
                    symvol = InBuffer[v];
                    CountBufferDebugIn = v;
                    //serialPort1.ReadTimeout = 10;//первый байт принят уменьшаем таймаут до минимума
                }
            }
            catch
            {
                if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                    result = 1;
                else
                    result = 0;
            }

            int a = 1;
        }
        //////////////////////////////////////////////////////////////////////////////
        uint crc32_update(uint crc, byte a)
        {
            int i;
            crc ^= a;
            for (i = 0; i < 8; ++i)
            {
                if ((crc & 1) > 0)
                    crc = (crc >> 1) ^ 0xEDB88320;
                else
                    crc = (crc >> 1);
            }
            return crc;
        }

        /////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////
        uint crc16_update(uint crc, byte a)
        {
            int i;

            crc ^= a;
            for (i = 0; i < 8; ++i)
            {
                if ((crc & 1) > 0)
                    crc = (crc >> 1) ^ 0xA001;
                else
                    crc = (crc >> 1);
            }

            return crc;
        }
        //////////////////////////////////////////////////////////////////////
        uint CRC_16(byte[] array, int len)
        {
            uint i = 0;
            uint crc = 0xffff;
            while (len > 0)
            {
                crc = crc16_update(crc, array[i]);
                i++;
                len--;
            }

            return crc;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                pathfile = openFileDialog1.FileName;
                textBox1.Text = openFileDialog1.SafeFileName;

            }

        }
        /////////////////////////////////////////////////////////////////////////   



    }
}