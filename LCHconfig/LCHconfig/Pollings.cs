﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Timers;


namespace LCHconfig
{
    public partial class Form1 : Form
    {


        //IAsyncResult result;

        //AutoResetEvent are1 = new AutoResetEvent(false);
        //AutoResetEvent are2 = new AutoResetEvent(false);
        //AutoResetEvent are3 = new AutoResetEvent(false);
        //AutoResetEvent are4 = new AutoResetEvent(false);
        //public static Mutex quemtxcom1 = new Mutex();

        //int bytesRec=0;
        //Queue<byte[]> Queue1 = new Queue<byte[]>();
        //Queue<byte[]> Queue2 = new Queue<byte[]>();
        //Queue<byte[]> Queue3 = new Queue<byte[]>();
        //Queue<byte[]> Queue4 = new Queue<byte[]>();

        private void Polling1()
        {
            while (true)
            {
                if (ask_verinfo == 1)
                {
                    ask_verinfo = 0;
                    ReadVerIvfo();
                }

                if (ask_rs == 1)
                {
                    ask_rs = 0;
                    Read485_422();
                }

                if (ask_lan == 1)
                {
                    ask_lan = 0;
                    ReadLan();
                    ReadDHCPLan_start = 1;
                }

                if (Write485_422_flag == 1)
                {
                    Write485_422_flag = 0;
                    if (Write485_422() == 0x01)
                    {
                        label16.Invoke(new Action<Color>(t => label16.ForeColor = t), System.Drawing.Color.LightGreen);
                        label16.Invoke(new Action(() => label16.Text = "Ok"));
                        //label16.ForeColor = System.Drawing.Color.LightGreen;
                        //label16.Text = "Ok";
                    }

                }

                if (WriteLan_flag == 1)
                {
                    WriteLan_flag = 0;
                    if (WriteLan() == 0x01)
                    {
                        label16.Invoke(new Action<Color>(t => label16.ForeColor = t), System.Drawing.Color.LightGreen);
                        label16.Invoke(new Action(() => label16.Text = "Ok"));
                        //label16.ForeColor = System.Drawing.Color.LightGreen;
                        //label16.Text = "Ok";
                        //Thread.Sleep(5000);
                        
                        ReadDHCPLan_start = 1;
                    }
                }

                if(ReadDHCPLan_start == 1)
                {
                    ReadDHCPLan_start = 0;
                    myTimer.Enabled = true;
                    myTimer.Start();
                }

                if (ReadDHCPLan_stop == 1)
                {
                    ReadDHCPLan_stop = 0;
                    myTimer.Stop();
                    ReadDHCPLan();
                }
               
                if (ask_serial_1 == 1)
                {
                    ask_serial_1 = 0;
                    ReadSerial(1, comboBox3, comboBox4, comboBox5, comboBox6);
                    ReadBaud(1, comboBox2);
                }

                if (ask_serial_2 == 1)
                {
                    ask_serial_2 = 0;
                    ReadSerial(2, comboBox10, comboBox9, comboBox8, comboBox7);
                    ReadBaud(2, comboBox11);
                }

                if (ask_serial_3 == 1)
                {
                    ask_serial_3 = 0;
                    ReadSerial(3, comboBox15, comboBox14, comboBox13, comboBox12);
                    ReadBaud(3, comboBox16);
                }

                if (ask_serial_4 == 1)
                {
                    ask_serial_4 = 0;
                    ReadSerial(4, comboBox20, comboBox19, comboBox18, comboBox17);
                    ReadBaud(4, comboBox21);
                }

                if (WriteSerial_flag_1 == 1)
                {
                    WriteSerial_flag_1 = 0;
                    if (WriteSerial(1, comboBox3, comboBox4, comboBox5, comboBox6) == 0x01)
                    {
                        if (WriteBaud(1, comboBox2)==1)
                        {
                            label16.Invoke(new Action<Color>(t => label16.ForeColor = t), System.Drawing.Color.LightGreen);
                            label16.Invoke(new Action(() => label16.Text = "Ok"));
                        }
                    }
                }

                if (WriteSerial_flag_2 == 1)
                {
                    WriteSerial_flag_2 = 0;
                    if (WriteSerial(2, comboBox10, comboBox9, comboBox8, comboBox7) == 0x01)
                    {
                        if (WriteBaud(2, comboBox11)==1)
                        {
                            label16.Invoke(new Action<Color>(t => label16.ForeColor = t), System.Drawing.Color.LightGreen);
                            label16.Invoke(new Action(() => label16.Text = "Ok"));
                        }

                    }
                }

                if (WriteSerial_flag_3 == 1)
                {
                    WriteSerial_flag_3 = 0;
                    if (WriteSerial(3, comboBox15, comboBox14, comboBox13, comboBox12) == 0x01)
                    {
                        if (WriteBaud(3, comboBox16)==1)
                        {
                            label16.Invoke(new Action<Color>(t => label16.ForeColor = t), System.Drawing.Color.LightGreen);
                            label16.Invoke(new Action(() => label16.Text = "Ok"));
                        }
                    }
                }

                if (WriteSerial_flag_4 == 1)
                {
                    WriteSerial_flag_4 = 0;
                    if (WriteSerial(4, comboBox20, comboBox19, comboBox18, comboBox17) == 0x01)
                    {
                        if (WriteBaud(4, comboBox21)==1)
                        {
                            label16.Invoke(new Action<Color>(t => label16.ForeColor = t), System.Drawing.Color.LightGreen);
                            label16.Invoke(new Action(() => label16.Text = "Ok"));
                        }
                    }
                }
                Thread.Sleep(5);
            }
        }


        private byte WriteBaud(int port, ComboBox comboBox1)
        {

            byte result = 0;
            byte flags = 0;
            string value = null;
            comboBox1.Invoke(new Action(() => value = comboBox1.Text));           
            int data = Convert.ToInt32(value);
            byte[] byteArray = BitConverter.GetBytes(data);

          

            NumberPacket++;
            byte[] InBuffer = new byte[1024];
            byte[] array_protocol = new byte[1024];
            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            //array_protocol[4] = (byte)((array_protocol.Length));
            //array_protocol[5] = (byte)((array_protocol.Length) >> 8);
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x03;//функция записи
            array_protocol[11] = 0x01;//количество индексированных команд
            array_protocol[12] = 0x2C;//Индекс команды
            array_protocol[13] = 0x03;//

            array_protocol[14] = (byte)port;//номер
            array_protocol[15] = 0x00;//структуры

            int dataln = 4;


            array_protocol[16] = (byte)dataln;//длинна 
            array_protocol[17] = (byte)(dataln >> 8);//данных


            array_protocol[18] = byteArray[0];
            array_protocol[19] = byteArray[1];
            array_protocol[20] = byteArray[2];
            array_protocol[21] = byteArray[3];


            int i = 22;
            array_protocol[4] = (byte)(i + 2);
            array_protocol[5] = (byte)((i + 2) >> 8);




            uint get_crc = CRC_16(array_protocol, i);
            array_protocol[i] = (byte)((get_crc));

            array_protocol[i + 1] = (byte)((get_crc) >> 8);

            try
            {
                serialPort1.Write(array_protocol, 0, i + 2);
            }
            catch
            {
                result = 0;
                return result;
            }
            serialPort1.ReadTimeout = 800;//
            try
            {
                for (byte v = 0; v < 255; v++)
                {
                    InBuffer[v] = (byte)serialPort1.ReadByte();
                    symvol = InBuffer[v];
                    CountBufferDebugIn = v;
                    serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                }
            }
            catch
            {
                if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                    result = 1;
                else
                    result = 0;
            }
            return result;

        }


        private byte WriteSerial(int port, ComboBox comboBox1, ComboBox comboBox2,
                                ComboBox comboBox3, ComboBox comboBox4)
        {

            byte result = 0;
            byte flags = 0;
            string value = null;
            comboBox1.Invoke(new Action(() =>  value = comboBox1.Text));

            if (value == "9")
                flags |= 0x02;



            string value11 = null;
            comboBox2.Invoke(new Action(() => value11 = comboBox2.Text));
            if (value11 == "0.5")
                flags |= 0x04;
            else
            if (value11 == "1.5")
                flags |= 0x08;
            else
            if (value11 == "2")
                flags |= 0x0C;






            string value12 = null;
            comboBox3.Invoke(new Action(() => value12 = comboBox3.Text));
            if (value12 == "Even")
                flags |= 0x10;
            else
            if (value12 == "Odd")
                flags |= 0x20;
            //else
            //if (value12 == "None")
            //    flags |= 0x30;



            string value13 = null;
            comboBox4.Invoke(new Action(() => value13 = comboBox4.Text));
            if (value13 == "RTS")
                flags |= 0x40;
            else
           if (value13 == "СTS")
                flags |= 0x80;
            else
           if (value13 == "RTS_CTS")
                flags |= 0xC0;


            //string value14 = textBox14.Text;
            //byte[] str14 = Encoding.Default.GetBytes(value14);

            ////string value1 = textBox6.Text;
            ////byte[] str1 = Encoding.Default.GetBytes(value1);

            NumberPacket++;
            byte[] InBuffer = new byte[1024];
            byte[] array_protocol = new byte[1024];
            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            //array_protocol[4] = (byte)((array_protocol.Length));
            //array_protocol[5] = (byte)((array_protocol.Length) >> 8);
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x03;//функция записи
            array_protocol[11] = 0x01;//количество индексированных команд
            array_protocol[12] = 0x2B;//Индекс команды
            array_protocol[13] = 0x03;//

            array_protocol[14] = (byte)port;//номер
            array_protocol[15] = 0x00;//структуры

            int dataln = 1;


            array_protocol[16] = (byte)dataln;//длинна 
            array_protocol[17] = (byte)(dataln >> 8);//данных

         
                array_protocol[18] = flags;
               

            int i = 19;
            array_protocol[4] = (byte)(i + 2);
            array_protocol[5] = (byte)((i + 2) >> 8);




            uint get_crc = CRC_16(array_protocol, i);
            array_protocol[i] = (byte)((get_crc));

            array_protocol[i + 1] = (byte)((get_crc) >> 8);

            try
            {
                serialPort1.Write(array_protocol, 0, i + 2);
            }
            catch
            {
                result = 0;
                return result;
            }
            serialPort1.ReadTimeout = 800;//
            try
            {
                for (byte v = 0; v < 255; v++)
                {
                    InBuffer[v] = (byte)serialPort1.ReadByte();
                    symvol = InBuffer[v];
                    CountBufferDebugIn = v;
                    serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                }
            }
            catch
            {
                if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                    result = 1;
                else
                    result = 0;
            }
            return result;

        }


        private void ReadBaud(int port, ComboBox comboBox1)
        {
            if (port == 1)
                Screen = MainMenu.SERIAL1;
            else
           if (port == 2)
                Screen = MainMenu.SERIAL2;
            else
           if (port == 3)
                Screen = MainMenu.SERIAL3;
            else
           if (port == 4)
                Screen = MainMenu.SERIAL4;

            byte[] InBuffer = new byte[1024];//
            char[] MidBuffer = new char[1024];//
            byte result = 0;
            NumberPacket++;



            byte[] array_protocol = new byte[18];//

            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            array_protocol[4] = (byte)((array_protocol.Length));
            array_protocol[5] = (byte)((array_protocol.Length) >> 8);
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x01;//функция чтения
            array_protocol[11] = 0x01;//количество индексированных команд
            array_protocol[12] = 0x2C;//Индекс команды
            array_protocol[13] = 0x03;//812 BAUD_UART
            array_protocol[14] = (byte)port;//номер структуры
            array_protocol[15] = 0x00;//


            uint get_crc = CRC_16(array_protocol, 16);
            array_protocol[16] = (byte)((get_crc));

            array_protocol[17] = (byte)((get_crc) >> 8);

            try
            {
                serialPort1.Write(array_protocol, 0, 18);
            }
            catch
            {

            }



            serialPort1.ReadTimeout = 800;//
            try
            {
                for (byte v = 0; v < 255; v++)
                {
                    InBuffer[v] = (byte)serialPort1.ReadByte();
                    //symvol = InBuffer[v];
                    CountBufferDebugIn = v;
                    serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                }
            }
            catch
            {
                if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x02))//ответ на чтение
                {

                    int ln = InBuffer[4];
                    ln |= InBuffer[5] << 8;

                    uint get_crc_in = CRC_16(InBuffer, ln - 2);
                    uint got_crc_in = InBuffer[ln - 2];
                    got_crc_in |= (uint)InBuffer[ln - 1] << 8;
                    if (got_crc_in == get_crc_in)
                        result = 1;
                    else
                        result = 0;
                }
                else
                    result = 0;
            }

            if (result == 1)
            {
                int baud = 0;
                     
                    baud = BitConverter.ToInt32(InBuffer, 18);                                  
              

                comboBox1.Invoke(new Action(() => comboBox1.Text = baud.ToString()));

            }

        }

        private void ReadSerial(int port, ComboBox comboBox1, ComboBox comboBox2, 
                                ComboBox comboBox3, ComboBox comboBox4)
        {

            if(port==1)
            Screen = MainMenu.SERIAL1;
            else
            if (port == 2)
            Screen = MainMenu.SERIAL2;
            else
            if (port == 3)
            Screen = MainMenu.SERIAL3;
            else
            if (port == 4)
            Screen = MainMenu.SERIAL4;
           

            byte[] InBuffer = new byte[1024];//
            char[] MidBuffer = new char[1024];//
            byte result = 0;
            NumberPacket++;



            byte[] array_protocol = new byte[18];//

            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            array_protocol[4] = (byte)((array_protocol.Length));
            array_protocol[5] = (byte)((array_protocol.Length) >> 8);
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x01;//функция чтения
            array_protocol[11] = 0x01;//количество индексированных команд
            array_protocol[12] = 0x2B;//Индекс команды
            array_protocol[13] = 0x03;//811 SET_UART
            
            array_protocol[14] = 0x00;//все
            array_protocol[15] = 0x00;//структуры


            uint get_crc = CRC_16(array_protocol, 16);
            array_protocol[16] = (byte)((get_crc));

            array_protocol[17] = (byte)((get_crc) >> 8);

            try
            {
                serialPort1.Write(array_protocol, 0, 18);
            }
            catch
            {

            }



            serialPort1.ReadTimeout = 800;//
            try
            {
                for (byte v = 0; v < 255; v++)
                {
                    InBuffer[v] = (byte)serialPort1.ReadByte();
                    //symvol = InBuffer[v];
                    CountBufferDebugIn = v;
                    serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                }
            }
            catch
            {
                if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x02))//ответ на чтение
                {

                    int ln = InBuffer[4];
                    ln |= InBuffer[5] << 8;

                    uint get_crc_in = CRC_16(InBuffer, ln - 2);
                    uint got_crc_in = InBuffer[ln - 2];
                    got_crc_in |= (uint)InBuffer[ln - 1] << 8;
                    if (got_crc_in == get_crc_in)
                        result = 1;
                    else
                        result = 0;
                }
                else
                    result = 0;
            }

            if (result == 1)
            {
                int flags = 0;
                if (port==1)
                   flags = InBuffer[18];
                if (port == 2)
                    flags = InBuffer[19];
                if (port == 3)
                    flags = InBuffer[20];
                if (port == 4)
                    flags = InBuffer[21];



                if ((flags & 0x02) == 02)//----------1-й бит 0 - USART_WordLength_8b, 1 - USART_WordLength_9b                  
                    comboBox1.Invoke(new Action(() => comboBox1.Text = "9"));
                else
                    comboBox1.Invoke(new Action(() => comboBox1.Text = "8"));


                int stopbits = ((flags & 0x0C) >> 2);
                if (stopbits == 0x00)                                          //----------2-й //бит 00 - USART_StopBits_1, 01 - USART_StopBits_0_5,
                    comboBox2.Invoke(new Action(() => comboBox2.Text = "1"));  //----------3-й //10 - USART_StopBits_1_5, 11 - USART_StopBits_2 
                else
                if (stopbits == 0x01)                                         
                    comboBox2.Invoke(new Action(() => comboBox2.Text = "0.5"));
                else
                if (stopbits == 0x02)
                    comboBox2.Invoke(new Action(() => comboBox2.Text = "1.5"));
                else
                if (stopbits == 0x03)
                    comboBox2.Invoke(new Action(() => comboBox2.Text = "2"));

                int parity = ((flags & 0x30) >> 4);//----------4-й //бит 00 - USART_Parity_No, 01 - USART_Parity_Even,
                                                   //----------5-й //10 - USART_Parity_Odd, 11 - USART_Parity_No  - этот 2 раза
                if (parity == 0x00)                                          
                    comboBox3.Invoke(new Action(() => comboBox3.Text = "None"));  
                else
                if (parity == 0x01)
                    comboBox3.Invoke(new Action(() => comboBox3.Text = "Even"));
                else
                if (parity == 0x02)
                    comboBox3.Invoke(new Action(() => comboBox3.Text = "Odd"));
                else
                if (parity == 0x03)
                    comboBox3.Invoke(new Action(() => comboBox3.Text = "None"));

                int flow = ((flags & 0xC0) >> 6);//----------6-й //бит 00 - USART_HardwareFlowControl_None, 01 - USART_HardwareFlowControl_RTS,
                                                 //----------7-й //10 - USART_HardwareFlowControl_СTS, 11 - USART_HardwareFlowControl_RTS_CTS

                if (flow == 0x00)
                    comboBox4.Invoke(new Action(() => comboBox4.Text = "None"));
                else
                if (flow == 0x01)
                    comboBox4.Invoke(new Action(() => comboBox4.Text = "RTS"));
                else
                if (flow == 0x02)
                    comboBox4.Invoke(new Action(() => comboBox4.Text = "СTS"));
                else
                if (flow == 0x03)
                    comboBox4.Invoke(new Action(() => comboBox4.Text = "RTS_CTS"));

              
            }

           

        }




        private byte ReadVerIvfo()
        {
            byte[] InBufferP = new byte[1024];
            byte[] MidBuffer = new byte[1024];
            int Count = 0;
            int bytesSent;
            int u = 0;
            long b = 0;
            long bb = 0;


            if (serialPort1.IsOpen)
            {

                try
                {

                    char[] msg = { '+', 'V', 'E', 'R', '_', 'I', 'N', 'F', 'O', '=', '?', '\r', '\n' };
                    byte[] sendmsg = new byte[13];
                    for (int i = 0; i < 13; i++)
                    {
                        sendmsg[i] = (byte)msg[i];
                    }
                    if (serialPort1.IsOpen)
                        serialPort1.Write(msg, 0, msg.Length);

                }
                catch (Exception e)
                {

                }

                try
                {
                    serialPort1.ReadTimeout = 800;
                    InBufferP[0] = (byte)serialPort1.ReadByte();
                }
                catch (Exception ex)
                {
                    Application.Exit();
                    //serialPort1.ReadTimeout = -1;
                    //continue;//здесь может быть исключение если выключают программу
                }
                Count++;
                //Thread.Sleep(200);//чтобы всё дошло..
                mtxcom1.WaitOne();
                try
                {
                    //while (serialPort1.BytesToRead != 0)
                    //{

                    //    byte[] data = new byte[serialPort1.BytesToRead];
                    //    serialPort1.Read(data, 0, data.Length);
                    //    Array.Copy(data, 0, InBufferP, 1, data.Length);
                    //    Count += data.Length;
                    //}
                    serialPort1.ReadTimeout = 300;
                    for (byte v = 1; v < 255; v++)
                    {
                        InBufferP[v] = (byte)serialPort1.ReadByte();
                        symvol = InBufferP[v];
                        CountBufferDebugIn = v;
                        //serialPort1.ReadTimeout = 10;//первый байт принят уменьшаем таймаут до минимума
                    }
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Не удалось получить ответ"); 
                    serialPort1.ReadTimeout = -1;
                    
                    //mtxcom1.ReleaseMutex();
                    //continue;
                }
                mtxcom1.ReleaseMutex();
                if (InBufferP[0] == '"')
                {
                    panel2.Invoke(new Action<bool>(t => panel2.Visible = t), true);
                    panel1.Invoke(new Action<bool>(t => panel1.Visible = t), false);
                    panel4.Invoke(new Action<bool>(t => panel4.Visible = t), false);
                    panel2.Invoke(new Action<bool>(t => panel4.Visible = t), false);
                    ask_rs = 1;
                    return 1;
                    //Thread.Sleep(500);
                }
                else
                    return 0;
                    //Thread.Sleep(500);
            }
            else
            {
                return 0;
                //Thread.Sleep(500);
                //Thread.Sleep(50);
            }
        }
    }
}