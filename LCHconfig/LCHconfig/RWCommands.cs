﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Net.Sockets;

namespace LCHconfig
{
    public partial class Form1 : Form
    {
        private void Read485_422()
        {
            Screen = MainMenu.RS;

            byte[] InBuffer = new byte[1024];//
            byte result = 0;
            NumberPacket++;

          

            byte[] array_protocol = new byte[18];//

            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            array_protocol[4] = (byte)((array_protocol.Length));
            array_protocol[5] = (byte)((array_protocol.Length) >> 8);
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x01;//функция чтения
            array_protocol[11] = 0x01;//количество индексированных команд
            array_protocol[12] = 0x2A;//Индекс команды
            array_protocol[13] = 0x03;//810
            array_protocol[14] = 0x01;//номер структуры
            array_protocol[15] = 0x00;//первый


            uint get_crc = CRC_16(array_protocol, 16);
            array_protocol[16] = (byte)((get_crc));

            array_protocol[17] = (byte)((get_crc) >> 8);

            try
            {
                serialPort1.Write(array_protocol, 0, 18);
            }
            catch
            {

            }
            serialPort1.ReadTimeout = 800;//
            try
            {
                for (byte v = 0; v < 255; v++)
                {
                    InBuffer[v] = (byte)serialPort1.ReadByte();
                    symvol = InBuffer[v];
                    CountBufferDebugIn = v;
                    serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                }
            }
            catch
            {
                if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x02))//ответ на чтение
                {

                    int ln = InBuffer[4];
                    ln |= InBuffer[5] << 8;

                    uint get_crc_in = CRC_16(InBuffer, ln - 2);
                    uint got_crc_in = InBuffer[ln - 2];
                    got_crc_in |= (uint)InBuffer[ln - 1] << 8;
                    if (got_crc_in == get_crc_in)
                        result = 1;
                    else
                        result = 0;
                }
                else
                    result = 0;
            }
            if (result == 1)
            {
                if (InBuffer[18] == 0x01)
                {
                    active485_notactive422();
                }
                else
                    if (InBuffer[18] == 0x00)
                    {
                        active422_notactive485();
                    }

                int tmp = InBuffer[19];
                tmp |= InBuffer[20] << 8;
                tmp |= InBuffer[21] << 16;
                tmp |= InBuffer[22] << 24;
                //textBox10.Text = tmp.ToString();
                textBox10.Invoke(new Action(() => textBox10.Text = tmp.ToString()));
                tmp = InBuffer[23];
                tmp |= InBuffer[24] << 8;
                tmp |= InBuffer[25] << 16;
                tmp |= InBuffer[26] << 24;
                //textBox9.Text = tmp.ToString();
                textBox9.Invoke(new Action(() => textBox9.Text = tmp.ToString()));
                tmp = InBuffer[27];
                tmp |= InBuffer[28] << 8;
                tmp |= InBuffer[29] << 16;
                tmp |= InBuffer[30] << 24;
                //textBox2.Text = tmp.ToString();
                textBox2.Invoke(new Action(() => textBox2.Text = tmp.ToString()));
                tmp = InBuffer[31];
                tmp |= InBuffer[32] << 8;
                tmp |= InBuffer[33] << 16;
                tmp |= InBuffer[34] << 24;
                //textBox3.Text = tmp.ToString();
                textBox3.Invoke(new Action(() => textBox3.Text = tmp.ToString()));
                tmp = InBuffer[35];
                tmp |= InBuffer[36] << 8;
                tmp |= InBuffer[37] << 16;
                tmp |= InBuffer[38] << 24;
                //textBox4.Text = tmp.ToString();
                textBox4.Invoke(new Action(() => textBox4.Text = tmp.ToString()));
                if (tmp == 0)
                    tmp++;
                //radioButton1
                if (InBuffer[39] == 1)
                {
                    //radioButton1.Checked = true;
                    //radioButton2.Checked = false;
                    radioButton1.Invoke(new Action<bool>(t => radioButton1.Checked = t), true);
                    radioButton2.Invoke(new Action<bool>(t => radioButton2.Checked = t), false);
                }
                else
                {
                    //radioButton2.Checked = true;
                    //radioButton1.Checked = false;
                    radioButton1.Invoke(new Action<bool>(t => radioButton1.Checked = t), false);
                    radioButton2.Invoke(new Action<bool>(t => radioButton2.Checked = t), true);
                }

            }
        }
        //serialPort1.BaudRate = int.Parse(comboBox1.SelectedItem.ToString());

        private byte Write485_422()
        {
            byte cp = 0;
            byte result = 0;
            if (textBox10.Enabled == true)
                cp = 1;
            int tmp1 = int.Parse(textBox10.Text);
            int tmp2 = int.Parse(textBox9.Text);
            int tmp3 = int.Parse(textBox2.Text);
            int tmp4 = int.Parse(textBox3.Text);
            int tmp5 = int.Parse(textBox4.Text);
            byte rb = 0;
            if (radioButton1.Checked == true)
                rb = 1;
            NumberPacket++;
            byte[] InBuffer = new byte[1024];
            byte[] array_protocol = new byte[42];
            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            array_protocol[4] = (byte)((array_protocol.Length));
            array_protocol[5] = (byte)((array_protocol.Length) >> 8);
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x03;//функция записи
            array_protocol[11] = 0x01;//количество индексированных команд
            array_protocol[12] = 0x2A;//Индекс команды
            array_protocol[13] = 0x03;//

            array_protocol[14] = 0x01;//номер
            array_protocol[15] = 0x00;//структуры

            array_protocol[16] = 0x16;//длинна 
            array_protocol[17] = 0x00;//данных 22

            array_protocol[18] = cp;
            array_protocol[19] = (byte)(0x000000FF & tmp1);
            array_protocol[20] = (byte)(0x000000FF & (tmp1 >> 8));
            array_protocol[21] = (byte)(0x000000FF & (tmp1 >> 16));
            array_protocol[22] = (byte)(0x000000FF & (tmp1 >> 24));

            array_protocol[23] = (byte)(0x000000FF & tmp2);
            array_protocol[24] = (byte)(0x000000FF & (tmp2 >> 8));
            array_protocol[25] = (byte)(0x000000FF & (tmp2 >> 16));
            array_protocol[26] = (byte)(0x000000FF & (tmp2 >> 24));

            array_protocol[27] = (byte)(0x000000FF & tmp3);
            array_protocol[28] = (byte)(0x000000FF & (tmp3 >> 8));
            array_protocol[29] = (byte)(0x000000FF & (tmp3 >> 16));
            array_protocol[30] = (byte)(0x000000FF & (tmp3 >> 24));

            array_protocol[31] = (byte)(0x000000FF & tmp4);
            array_protocol[32] = (byte)(0x000000FF & (tmp4 >> 8));
            array_protocol[33] = (byte)(0x000000FF & (tmp4 >> 16));
            array_protocol[34] = (byte)(0x000000FF & (tmp4 >> 24));

            array_protocol[35] = (byte)(0x000000FF & tmp5);
            array_protocol[36] = (byte)(0x000000FF & (tmp5 >> 8));
            array_protocol[37] = (byte)(0x000000FF & (tmp5 >> 16));
            array_protocol[38] = (byte)(0x000000FF & (tmp5 >> 24));
            array_protocol[39] = rb;

            uint get_crc = CRC_16(array_protocol, 40);
            array_protocol[40] = (byte)((get_crc));

            array_protocol[41] = (byte)((get_crc) >> 8);

            try
            {
                serialPort1.Write(array_protocol, 0, 42);
            }
            catch
            {
                result = 0;
                return result;
            }
            serialPort1.ReadTimeout = 800;//
            try
            {
                for (byte v = 0; v < 255; v++)
                {
                    InBuffer[v] = (byte)serialPort1.ReadByte();
                    symvol = InBuffer[v];
                    CountBufferDebugIn = v;
                    serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                }
            }
            catch
            {
                if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                    result = 1;
                else
                    result = 0;
            }
            return result;

        }


        private void ReadLan()
        {
            Screen = MainMenu.LAN;

            byte[] InBuffer = new byte[1024];//
            char[] MidBuffer = new char[1024];//
            byte result = 0;
            NumberPacket++;

          

            byte[] array_protocol = new byte[18];//

            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            array_protocol[4] = (byte)((array_protocol.Length));
            array_protocol[5] = (byte)((array_protocol.Length) >> 8);
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x01;//функция чтения
            array_protocol[11] = 0x01;//количество индексированных команд
            array_protocol[12] = 0xDC;//Индекс команды
            array_protocol[13] = 0xFF;//65500
            array_protocol[14] = 0x01;//номер структуры
            array_protocol[15] = 0x00;//первый


            uint get_crc = CRC_16(array_protocol, 16);
            array_protocol[16] = (byte)((get_crc));

            array_protocol[17] = (byte)((get_crc) >> 8);

            try
            {
                serialPort1.Write(array_protocol, 0, 18);
            }
            catch
            {

            }



            serialPort1.ReadTimeout = 800;//
            try
            {
                for (byte v = 0; v < 255; v++)
                {
                    InBuffer[v] = (byte)serialPort1.ReadByte();
                    //symvol = InBuffer[v];
                    CountBufferDebugIn = v;
                    serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                }
            }
            catch
            {
                if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x02))//ответ на чтение
                {

                    int ln = InBuffer[4];
                    ln |= InBuffer[5] << 8;

                    uint get_crc_in = CRC_16(InBuffer, ln - 2);
                    uint got_crc_in = InBuffer[ln - 2];
                    got_crc_in |= (uint)InBuffer[ln - 1] << 8;
                    if (got_crc_in == get_crc_in)
                        result = 1;
                    else
                        result = 0;
                }
                else
                    result = 0;
            }

            if (result == 1)
            {
                if (InBuffer[18] == 0x01)//получать dhcp
                {
                    //checkBox1.Checked = true;
                    checkBox1.Invoke(new Action(() => checkBox1.Checked = true));
                }
                else
                    if (InBuffer[18] == 0x00)
                    {
                        //checkBox1.Checked = false;
                        checkBox1.Invoke(new Action(() => checkBox1.Checked = false));
                    }
                Array.Copy(InBuffer, 19, MidBuffer, 0, 100);
                //StringBuilder sb = new StringBuilder(InBuffer.Length);
                //string value = sb.Append(MidBuffer).ToString();
                int i = 0;
                for( ;i<15;i++)
                {
                    if (MidBuffer[i] == 0)
                        break;
                }

                char[] IP = new char[i];
                Array.Copy(MidBuffer, 0, IP, 0, i);
                string value = String.Concat<char>(IP);

                //textBox5.Text = value;
                textBox5.Invoke(new Action(() => textBox5.Text = value));
                i++;
                int shift = i;
                for (; i < 15 + shift; i++)
                {
                    if (MidBuffer[i] == 0)
                        break;
                }
                char[] MASKA = new char[i-shift];
                Array.Copy(MidBuffer, shift, MASKA, 0, i - shift);
                string maska = String.Concat<char>(MASKA);
                //textBox11.Text = maska;
                textBox11.Invoke(new Action(() => textBox11.Text = maska));

                i++;
                shift = i;
                for (; i < 15 + shift; i++)
                {
                    if (MidBuffer[i] == 0)
                        break;
                }
                char[] SHLYUZ = new char[i - shift];
                Array.Copy(MidBuffer, shift, SHLYUZ, 0, i - shift);
                string shlyuz = String.Concat<char>(SHLYUZ);
                //textBox12.Text = shlyuz;
                textBox12.Invoke(new Action(() => textBox12.Text = shlyuz));
                i++;
                shift = i;
                for (; i < 15 + shift; i++)
                {
                    if (MidBuffer[i] == 0)
                        break;
                }
                char[] DNS1 = new char[i - shift];
                Array.Copy(MidBuffer, shift, DNS1, 0, i - shift);
                string dns1 = String.Concat<char>(DNS1);
                //textBox13.Text = dns1;
                textBox13.Invoke(new Action(() => textBox13.Text = dns1));
                i++;
                shift = i;
                for (; i < 15 + shift; i++)
                {
                    if (MidBuffer[i] == 0)
                        break;
                }
                char[] DNS2 = new char[i - shift];
                Array.Copy(MidBuffer, shift, DNS2, 0, i - shift);
                string dns2 = String.Concat<char>(DNS2);
                //textBox14.Text = dns2;
                textBox14.Invoke(new Action(() => textBox14.Text = dns2));
            }

                NumberPacket++;
                array_protocol[0] = 0x80;
                array_protocol[1] = 0x80;
                array_protocol[2] = 0x80;
                array_protocol[3] = 0x01;
                array_protocol[4] = (byte)((array_protocol.Length));
                array_protocol[5] = (byte)((array_protocol.Length) >> 8);
                array_protocol[6] = 0xFA;
                array_protocol[7] = 0xFF;
                array_protocol[8] = (byte)((NumberPacket));
                array_protocol[9] = (byte)((NumberPacket) >> 8);
                array_protocol[10] = 0x01;//функция чтения
                array_protocol[11] = 0x01;//количество индексированных команд
                array_protocol[12] = 0xDE;//Индекс команды
                array_protocol[13] = 0xFF;//65502
                array_protocol[14] = 0x01;//номер структуры
                array_protocol[15] = 0x00;//первый


                get_crc = CRC_16(array_protocol, 16);
                array_protocol[16] = (byte)((get_crc));

                array_protocol[17] = (byte)((get_crc) >> 8);

                try
                {
                    serialPort1.Write(array_protocol, 0, 18);
                }
                catch
                {

                }

                serialPort1.ReadTimeout = 800;//
                try
                {
                    for (byte v = 0; v < 255; v++)
                    {
                        InBuffer[v] = (byte)serialPort1.ReadByte();
                        //symvol = InBuffer[v];
                        CountBufferDebugIn = v;
                        serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                    }
                }
                catch
                {
                    if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x02))//ответ на чтение
                    {

                        int ln = InBuffer[4];
                        ln |= InBuffer[5] << 8;

                        uint get_crc_in = CRC_16(InBuffer, ln - 2);
                        uint got_crc_in = InBuffer[ln - 2];
                        got_crc_in |= (uint)InBuffer[ln - 1] << 8;
                        if (got_crc_in == get_crc_in)
                            result = 1;
                        else
                            result = 0;
                    }
                    else
                        result = 0;
                }


             if (result == 1)
            {
                 uint tmp = InBuffer[18];
                      tmp|= (uint)InBuffer[19]<<8;                
                // textBox6.Text = tmp.ToString();
                textBox6.Invoke(new Action(() => textBox6.Text = tmp.ToString()));

            }
               
            }
        private void ReadDHCPLan()
        {
            Screen = MainMenu.LAN;

            byte[] InBuffer = new byte[1024];//
            char[] MidBuffer = new char[1024];//
            byte result = 0;
            NumberPacket++;



            byte[] array_protocol = new byte[18];//

            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            array_protocol[4] = (byte)((array_protocol.Length));
            array_protocol[5] = (byte)((array_protocol.Length) >> 8);
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x01;//функция чтения
            array_protocol[11] = 0x01;//количество индексированных команд
            array_protocol[12] = 0xDD;//Индекс команды
            array_protocol[13] = 0xFF;//65500
            array_protocol[14] = 0x01;//номер структуры
            array_protocol[15] = 0x00;//первый


            uint get_crc = CRC_16(array_protocol, 16);
            array_protocol[16] = (byte)((get_crc));

            array_protocol[17] = (byte)((get_crc) >> 8);

            try
            {
                serialPort1.Write(array_protocol, 0, 18);
            }
            catch
            {

            }



            serialPort1.ReadTimeout = 800;//
            try
            {
                for (byte v = 0; v < 255; v++)
                {
                    InBuffer[v] = (byte)serialPort1.ReadByte();
                    //symvol = InBuffer[v];
                    CountBufferDebugIn = v;
                    serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                }
            }
            catch
            {
                if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x02))//ответ на чтение
                {

                    int ln = InBuffer[4];
                    ln |= InBuffer[5] << 8;

                    uint get_crc_in = CRC_16(InBuffer, ln - 2);
                    uint got_crc_in = InBuffer[ln - 2];
                    got_crc_in |= (uint)InBuffer[ln - 1] << 8;
                    if (got_crc_in == get_crc_in)
                        result = 1;
                    else
                        result = 0;
                }
                else
                    result = 0;
            }

            if (result == 1)
            {
                //if (InBuffer[18] == 0x01)//получать dhcp
                //{
                //    checkBox1.Checked = true;
                //}
                //else
                //    if (InBuffer[18] == 0x00)
                //{
                //    checkBox1.Checked = false;
                //}
                Array.Copy(InBuffer, 19, MidBuffer, 0, 100);
                //StringBuilder sb = new StringBuilder(InBuffer.Length);
                //string value = sb.Append(MidBuffer).ToString();
                int i = 0;
                for (; i < 15; i++)
                {
                    if (MidBuffer[i] == 0)
                        break;
                }

                char[] IP = new char[i];
                Array.Copy(MidBuffer, 0, IP, 0, i);
                string value = String.Concat<char>(IP);

                //textBox5.Text = value;
                textBox15.Invoke(new Action(() => textBox15.Text = value));
                //i++;
                //int shift = i;
                //for (; i < 15 + shift; i++)
                //{
                //    if (MidBuffer[i] == 0)
                //        break;
                //}
                //char[] MASKA = new char[i - shift];
                //Array.Copy(MidBuffer, shift, MASKA, 0, i - shift);
                //string maska = String.Concat<char>(MASKA);
                ////textBox11.Text = maska;
                //textBox11.Invoke(new Action(() => textBox11.Text = maska));

                //i++;
                //shift = i;
                //for (; i < 15 + shift; i++)
                //{
                //    if (MidBuffer[i] == 0)
                //        break;
                //}
                //char[] SHLYUZ = new char[i - shift];
                //Array.Copy(MidBuffer, shift, SHLYUZ, 0, i - shift);
                //string shlyuz = String.Concat<char>(SHLYUZ);
                ////textBox12.Text = shlyuz;
                //textBox12.Invoke(new Action(() => textBox12.Text = shlyuz));
                //i++;
                //shift = i;
                //for (; i < 15 + shift; i++)
                //{
                //    if (MidBuffer[i] == 0)
                //        break;
                //}
                //char[] DNS1 = new char[i - shift];
                //Array.Copy(MidBuffer, shift, DNS1, 0, i - shift);
                //string dns1 = String.Concat<char>(DNS1);
                ////textBox13.Text = dns1;
                //textBox13.Invoke(new Action(() => textBox13.Text = dns1));
                //i++;
                //shift = i;
                //for (; i < 15 + shift; i++)
                //{
                //    if (MidBuffer[i] == 0)
                //        break;
                //}
                //char[] DNS2 = new char[i - shift];
                //Array.Copy(MidBuffer, shift, DNS2, 0, i - shift);
                //string dns2 = String.Concat<char>(DNS2);
                ////textBox14.Text = dns2;
                //textBox14.Invoke(new Action(() => textBox14.Text = dns2));
            }

            //NumberPacket++;
            //array_protocol[0] = 0x80;
            //array_protocol[1] = 0x80;
            //array_protocol[2] = 0x80;
            //array_protocol[3] = 0x01;
            //array_protocol[4] = (byte)((array_protocol.Length));
            //array_protocol[5] = (byte)((array_protocol.Length) >> 8);
            //array_protocol[6] = 0xFA;
            //array_protocol[7] = 0xFF;
            //array_protocol[8] = (byte)((NumberPacket));
            //array_protocol[9] = (byte)((NumberPacket) >> 8);
            //array_protocol[10] = 0x01;//функция чтения
            //array_protocol[11] = 0x01;//количество индексированных команд
            //array_protocol[12] = 0xDE;//Индекс команды
            //array_protocol[13] = 0xFF;//65502
            //array_protocol[14] = 0x01;//номер структуры
            //array_protocol[15] = 0x00;//первый


            //get_crc = CRC_16(array_protocol, 16);
            //array_protocol[16] = (byte)((get_crc));

            //array_protocol[17] = (byte)((get_crc) >> 8);

            //try
            //{
            //    serialPort1.Write(array_protocol, 0, 18);
            //}
            //catch
            //{

            //}

            //serialPort1.ReadTimeout = 800;//
            //try
            //{
            //    for (byte v = 0; v < 255; v++)
            //    {
            //        InBuffer[v] = (byte)serialPort1.ReadByte();
            //        //symvol = InBuffer[v];
            //        CountBufferDebugIn = v;
            //        serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
            //    }
            //}
            //catch
            //{
            //    if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x02))//ответ на чтение
            //    {

            //        int ln = InBuffer[4];
            //        ln |= InBuffer[5] << 8;

            //        uint get_crc_in = CRC_16(InBuffer, ln - 2);
            //        uint got_crc_in = InBuffer[ln - 2];
            //        got_crc_in |= (uint)InBuffer[ln - 1] << 8;
            //        if (got_crc_in == get_crc_in)
            //            result = 1;
            //        else
            //            result = 0;
            //    }
            //    else
            //        result = 0;
            //}


            //if (result == 1)
            //{
            //    uint tmp = InBuffer[18];
            //    tmp |= (uint)InBuffer[19] << 8;
            //    // textBox6.Text = tmp.ToString();
            //    textBox6.Invoke(new Action(() => textBox6.Text = tmp.ToString()));

            //}

        }


        private byte WriteLan()
        {
            byte result = 0;
            string value = textBox5.Text;
            byte[] str = Encoding.Default.GetBytes(value);

            string value11 = textBox11.Text;
            byte[] str11 = Encoding.Default.GetBytes(value11);

            string value12 = textBox12.Text;
            byte[] str12 = Encoding.Default.GetBytes(value12);

            string value13 = textBox13.Text;
            byte[] str13 = Encoding.Default.GetBytes(value13);

            string value14 = textBox14.Text;
            byte[] str14 = Encoding.Default.GetBytes(value14);

            //string value1 = textBox6.Text;
            //byte[] str1 = Encoding.Default.GetBytes(value1);

            NumberPacket++;
            byte[] InBuffer = new byte[1024];
            byte[] array_protocol = new byte[1024];
            array_protocol[0] = 0x80;
            array_protocol[1] = 0x80;
            array_protocol[2] = 0x80;
            array_protocol[3] = 0x01;
            //array_protocol[4] = (byte)((array_protocol.Length));
            //array_protocol[5] = (byte)((array_protocol.Length) >> 8);
            array_protocol[6] = 0xFA;
            array_protocol[7] = 0xFF;
            array_protocol[8] = (byte)((NumberPacket));
            array_protocol[9] = (byte)((NumberPacket) >> 8);
            array_protocol[10] = 0x03;//функция записи
            array_protocol[11] = 0x01;//количество индексированных команд
            array_protocol[12] = 0xDC;//Индекс команды
            array_protocol[13] = 0xFF;//

            array_protocol[14] = 0x01;//номер
            array_protocol[15] = 0x00;//структуры

            int dataln = 1 + 1 + str.Length + 1 + str11.Length + 1 + str12.Length + 1 + str13.Length + 1 + str14.Length;


            array_protocol[16] = (byte)dataln;//длинна 
            array_protocol[17] = (byte)(dataln >> 8);//данных
 
            if(checkBox1.Checked == true)
            array_protocol[18] = 0x01;//dhcp
            else
                array_protocol[18] = 0x00;//dhcp

            int i = 19;
            for ( ; i < str.Length + 19; i++)
            {
                array_protocol[i] = str[i-19];
            }

            array_protocol[i] = 0;
            i++;
            int shift = i;

            for (; i < str11.Length + shift; i++)
            {
                array_protocol[i] = str11[i - shift];
            }

            array_protocol[i] = 0;
            i++;

            shift = i;

            for (; i < str12.Length + shift; i++)
            {
                array_protocol[i] = str12[i - shift];
            }

            array_protocol[i] = 0;
            i++;

            shift = i;

            for (; i < str13.Length + shift; i++)
            {
                array_protocol[i] = str13[i - shift];
            }

            array_protocol[i] = 0;
            i++;

            shift = i;

            for (; i < str14.Length + shift; i++)
            {
                array_protocol[i] = str14[i - shift];
            }

            array_protocol[i] = 0;
            i++;


            array_protocol[4] = (byte)(i + 2);
            array_protocol[5] = (byte)((i + 2) >> 8);




            uint get_crc = CRC_16(array_protocol, i);
            array_protocol[i] = (byte)((get_crc));

            array_protocol[i+1] = (byte)((get_crc) >> 8);

            try
            {
                serialPort1.Write(array_protocol, 0, i+2);
            }
            catch
            {
                result = 0;
                return result;
            }
            serialPort1.ReadTimeout = 800;//
            try
            {
                for (byte v = 0; v < 255; v++)
                {
                    InBuffer[v] = (byte)serialPort1.ReadByte();
                    symvol = InBuffer[v];
                    CountBufferDebugIn = v;
                    serialPort1.ReadTimeout = 300;//первый байт принят уменьшаем таймаут до минимума
                }
            }
            catch
            {
                if ((InBuffer[0] == 0x80) && (InBuffer[1] == 0x80) && (InBuffer[2] == 0x80) && (InBuffer[10] == 0x04))//подтверждение записи 
                    result = 1;
                else
                    result = 0;
            }
            return result;

        }
        }


    
}